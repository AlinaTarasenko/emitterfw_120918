/*
 * lidarManager.cpp
 *
 *  Created on: Dec 4, 2017
 *      Author: Igor
 */

#include "lidarManager.h"

LidarManager::LidarManager(I2C &i2cRef, PinName sh1, PinName sh2, PinName sh3, Serial &serialRef)
:  lidar1(i2cRef, timer, serialRef),
   lidar2(i2cRef, timer, serialRef),
   lidar3(i2cRef, timer, serialRef),
   shutDown1(sh1),
   shutDown2(sh2),
   shutDown3(sh3)
{
	// ADDRESS_DEFAULT (0b0101001 << 1)
	address1 = 0x2A << 1;
	address2 = 0x2C << 1;
	address3 = 0x2E << 1;

	data1 = 0;
	data2 = 0;
	data3 = 0;

	timer.start();
	shutDown1 = 0;
	shutDown2 = 0;
	shutDown3 = 0;


}

void LidarManager::init(){

	shutDown1 = 1;
	wait_ms(50);
	lidar1.setAddress(address1);
	lidar1.setTimeout(100);
	lidar1.init();

	shutDown2 = 1;
	wait_ms(50);
	lidar2.setAddress(address2);
	lidar2.setTimeout(100);
	lidar2.init();

	shutDown3 = 1;
	wait_ms(50);
	lidar3.setAddress(address3);
	lidar3.setTimeout(100);
	lidar3.init();

}


void LidarManager::startContinuous(){
	lidar1.stopContinuous();
	lidar2.stopContinuous();
	lidar3.stopContinuous();
}


void LidarManager::stopContinuous(){
	lidar1.stopContinuous();
	lidar2.stopContinuous();
	lidar3.stopContinuous();
}


int LidarManager::readContinuous(){
	data1 = lidar1.readRangeContinuousMillimeters();
	data2 = lidar2.readRangeContinuousMillimeters();
	data3 = lidar3.readRangeContinuousMillimeters();

	return min(data1, min(data2, data3));
}


int LidarManager::readSingle(){
	data1 = lidar1.readRangeSingleMillimeters();
	data2 = lidar2.readRangeSingleMillimeters();
	data3 = lidar3.readRangeSingleMillimeters();

	return min(data1, min(data2, data3));
}
