#include "buttons.h"


Buttons::Buttons(PinName t, PinName b1, PinName b2, PinName b3) : _trigger(t), _button1(b1), _button2(b2), _button3(b3) {}

void Buttons::inputInit(){
    
    for(int i = 0; i < 4; i++){inputHistory[i] = 0;};
    for(int i = 0; i < 4; i++){inputStates[i] = 0; prevInput[i] = 0;};
    
    //inputMask = 0b1111000000111111;
    //inputHi =   0b0000000000111110;
    //inputLo =   0b1111000000000000;
    //inputRaw = 0;
    
    _trigger.mode(PullDown);
    _button1.mode(PullDown);
    _button2.mode(PullDown);
    _button3.mode(PullDown);
    
    holdTimer.start();
}

void Buttons::pollInputs(Serial &debug){    
    
    inputHistory[0] |= _trigger;
    inputHistory[1] |= _button1;
    inputHistory[2] |= _button2;
    inputHistory[3] |= _button3;
    
    for(int i = 0; i < 4; i++){inputHistory[i] = inputHistory[i] << 1;};
    
}

uint8_t Buttons::updateInputs(Serial &debug){
    uint8_t inputTemp = 0;
    
    for (int i = 0; i < 4; i++){
        
        
        //if( (inputHistory[i] & inputMask) == inputHi)  {  /* inputHistory[i] = 0b1111111111111111;  */   inputStates[i] = 1;}          // pressed = 2
        //if( (inputHistory[i] & inputMask) == inputLo)  {  /* inputHistory[i] = 0b0000000000000000;  */   inputStates[i] = 0;}     // released = 3
        if(inputHistory[i] == 0xFFFE) {inputStates[i] = 1;}                                               // down = 1
        if(inputHistory[i] == 0x0) {inputStates[i] = 0;}                                               // up = 0
        //else {}
    }
    
    for (int i = 0; i < 4; i++){
        inputTemp |= inputStates[i];
        inputTemp = inputTemp << 2;
    }
    
    
    
    return inputTemp;    
}
    
int Buttons::trigger(){
    
    int output;
    
    if( inputStates[0] == 0 && prevInput[0] == 0){ output = 0; }    // off 
    if( inputStates[0] == 1 && prevInput[0] == 1){ output = 1; }    // on
    if( inputStates[0] == 1 && prevInput[0] == 0){ output = 2;  holdTimer.reset();}    // pressed
    if( inputStates[0] == 0 && prevInput[0] == 1){ output = 3; }    // released
    prevInput[0] = inputStates[0];    
    
    if(output == 1 and holdTimer.read() > 1){ 
        return 4;
    }
    else{
        return output;
    }

}

int Buttons::button1(){
    
    int output;
    
    if( inputStates[1] == 0 && prevInput[1] == 0){ output = 0; }    
    if( inputStates[1] == 1 && prevInput[1] == 1){ output = 1; }    
    if( inputStates[1] == 1 && prevInput[1] == 0){ output = 2;  holdTimer.reset();}
    if( inputStates[1] == 0 && prevInput[1] == 1){ output = 3; }        
    prevInput[1] = inputStates[1];    
    
    if(output == 1 and holdTimer.read() > 1){ 
        return 4;
    }
    else{
        return output;
    }
    

}


/*

if button1() == 2
    start timer
    
if button1() == 1 and timer > 1
    output 1
    
else output 0


*/


int Buttons::button2(){
    int output;
    
    if( inputStates[2] == 0 && prevInput[2] == 0){ output = 0; }    
    if( inputStates[2] == 1 && prevInput[2] == 1){ output = 1; }    
    if( inputStates[2] == 1 && prevInput[2] == 0){ output = 2;  holdTimer.reset();}
    if( inputStates[2] == 0 && prevInput[2] == 1){ output = 3; }        
    prevInput[2] = inputStates[2];    
    
    if(output == 1 and holdTimer.read() > 1){ 
        return 4;
    }
    else{
        return output;
    }
}
    
int Buttons::button3(){
    int output;
    
    if( inputStates[3] == 0 && prevInput[3] == 0){ output = 0; }    
    if( inputStates[3] == 1 && prevInput[3] == 1){ output = 1; }    
    if( inputStates[3] == 1 && prevInput[3] == 0){ output = 2; holdTimer.reset();}
    if( inputStates[3] == 0 && prevInput[3] == 1){ output = 3; }      
    prevInput[3] = inputStates[3];    
    
    if(output == 1 and holdTimer.read() > 1){ 
        return 4;
    }
    else{
        return output;
    }
}    




