#ifndef SYSTEM_DATA_H
#define SYSTEM_DATA_H

#define INTERNAL_ERROR_INIT 		0x1
#define INTERNAL_ERROR_COLLIMATOR 	0x2
#define INTERNAL_ERROR_IMU 		0x4
#define INTERNAL_ERROR_WATCHDOG 	0x8

#define ERROR_TEMP			0x1
#define ERROR_HV_COMMUNICATION		0x2
#define ERROR_DET_COMMUNICATION		0x4
#define ERROR_TRACKING			0x8
#define ERROR_CLEAR_INTERNAL		0x80

#include "mbed.h"

class SystemData{

public:
    SystemData();

    // internally set values:
    int systemState;
    // x ray settings
    int xray_kV; 
    int xray_mA;
    int xray_mS;    

    // lidar settings
    uint16_t lidarData;
    uint16_t lidarLimit;    
    
    // outgoing data
    float tubeTemp;
    float tempLimit;
    bool tubeOverTemp;

    float collimator_position;
    int emitterIdle;
    int ledState;
    uint8_t internalErrors;


    /*
     * Internal errors -
     * 		0b00000001 - Init error
     * 		0b00000010 - Collimator timeout
     * 		0b00000100 - IMU
     * 		0b00001000 - Watchdog reset
     *
     */

    // received values
    int mode;
    int xrayState;
    /*
     * x ray state -
     * 		set by logic running on NUC
     * 		0 = xray disarmed
     * 		1 = standby
     * 		2 = armed
     * 		3 = active
     */
    int xray_Kv_echo; 
    int xray_mA_echo;
    int xray_mS_echo;
    int collimator_setpoint;
    uint8_t receivedErrors;
    
    int messageDiff;
    int validMessage;
    int connected;
    
    int debugId;
    char debugMessage[32];
    

    /*
    error code definition
    1 - tube over temp
    2 - HV power supply communication
    4 - Detector communication error
    8 - Tracking error
    16 - TBD
    32 - TBD
    64 - TBD
    128 - TBD
    */
    
};


#endif
