#ifndef BUTTONS_H
#define BUTTONS_H

#include "mbed.h"

#define b1111111111111110 0xFFFE
class Buttons{
    
    public:
    Buttons(PinName, PinName, PinName, PinName);
    
    void inputInit();       // Set all input modes
    void pollInputs(Serial&);      // Attached to timer
    uint8_t updateInputs(Serial&); // Run in main loop
    
    int trigger();
    int button1();
    int button2();
    int button3();
    
    int hold3();
    
    //private:
    //uint8_t inputRaw;
    uint16_t inputHistory[4];
    //uint16_t inputMask;

    //uint16_t inputHi;
    //uint16_t inputLo;

    uint8_t prevInput[4];
    uint8_t inputStates[4];    
    
    DigitalIn _trigger;
    DigitalIn _button1;
    DigitalIn _button2;
    DigitalIn _button3;
    
    Timer holdTimer;
    
    
    
};

#endif
