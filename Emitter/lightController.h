#ifndef LIGHT_CONTROLLER_H
#define LIGHT_CONTROLLER_H

#include "mbed.h"
#include "systemData.h"


class LightControl{
public:
	// 			 Laser	  Red	   Green	Blue
    LightControl(PinName, PinName, PinName, PinName, SystemData&);
    
    void update();
    
    void setLaser();
    void setPattern(int);    
    

private:

    // 				r, g, b, a
    void setColors(float, float, float, float);

    int laserMode;
    int patternMode;
    int prevPattern;
    int blink;
    
    int blinkState;
    int blinkTime;
    
    
    //uint32_t red;
    //uint32_t blue;
    //uint32_t green;
    //uint32_t yellow;
    //uint32_t stripArray1[26];
    //uint32_t stripArray2[26];
    //uint32_t stripArray3[26];
    
    
    /*
    
    
    000 Off         
    
    001 Red
    010 Yellow
    011 Green
    
    100 Blue
    101 Blue/Green
    110 Blue/Yellow
    111 Blue/Red
    */

    PwmOut laserEn;
    PwmOut redEn;
    PwmOut greenEn;
    PwmOut blueEn;
    
    SystemData &sysData;
    Timer lightTimer;
    
    
};

#endif
