#include "commHandler.h"


CommHandler::CommHandler(Serial &commRef, SystemData &sysRef) : comm(commRef), sysData(sysRef) {
    for(int i = 0; i < 265; i++){
        buffer[i] = '\0';
        txMessage[i] = '\0';
    }

    index = 0;
    txIndex = 0;
    validMessage = 0;

    lockOut = false;

    messageId = 0;
    recivedId = 0;

    connected = 0;
    portActive = 0;
    messageAvalible = 0;
    txTime.start();
    timeOut.start();
    timeOutRx.start();
    
}


// might miss message start
void CommHandler::bufferIrq(){
    //sprintf(sysData.debugMessage, "0300");
    
    timeOutRx.reset();
    if(comm.readable()){
        while(comm.readable() /*&& messageAvalible == 0*/ ){
        
            if(timeOutRx.read_ms() > 500){
            	break;
            }
        
            incomingCh = comm.getc();
           // sprintf(sysData.debugMessage, "30-%c", incomingCh);
        
            if(incomingCh == '#'){
                //sprintf(sysData.debugMessage, "0301");
                index = 0;
            }
            else if(incomingCh == '~'){
                buffer[index] = '\0';
                messageAvalible = 1;
                //sprintf(sysData.debugMessage, "0302");
            }
            else{
                buffer[index] = incomingCh;
                //sprintf(sysData.debugMessage, "0303");
                if(index++ > 255){index = 0;}
            
            }
            //sprintf(sysData.debugMessage, "0304");           
        }
    }
    //sprintf(sysData.debugMessage, "305-%i", sysData.debugId++);
}


// Copy values from 'buffer' to 'data' and reset/clear buffer
void CommHandler::getBuffer(){
    
    incoming = string(buffer);
    
    //dataLength = index;
    
    //messageAvalible = 0;
}

// returns dataAvalible state
int CommHandler::checkBuffer(){
    return messageAvalible;
}


int CommHandler::validate(){
    
    if( (messageId - recivedId) < 10 ){
        return 1;
    }
    else{
        return 0;
    }   
}


/*

Return messge format:

#
recived messageId
mode
xray state
xray kv echo
xray mA echo
xray uS echo
error code
~

*/

int CommHandler::parse(){
    
    //NVIC_DisableIRQ(USART2_IRQn);
    
    getBuffer();
    values.clear();
    //comm.printf("parsing \n\r");
    
    timeOut.reset();
    
    while (incoming.length() > 0) {
        
        if(timeOut.read_ms() > 500){return 0;}
        
        size_t pos = incoming.find(",");
        string value = incoming.substr(0, pos);
        //sprintf(sysData.debugMessage, "0101");

        if ((int)pos < 0) {
            value = incoming;
            incoming.clear();
            //sprintf(sysData.debugMessage, "0102");
        } else {
            incoming = incoming.substr(pos + 1);
            
        }

        //sprintf(sysData.debugMessage, "0103");
        values.push_back(atoi(value.c_str()));
        //sprintf(sysData.debugMessage, "0104");

    }
    
    if(values.size() == 8){
        recivedId = values[0];
        sysData.mode = values[1];
        sysData.xrayState = values[2];
        sysData.xray_Kv_echo = values[3];
        sysData.xray_mA_echo = values[4];
        sysData.xray_mS_echo = values[5];
        sysData.collimator_setpoint = values[6];
        sysData.receivedErrors = values[7];
        //sprintf(sysData.debugMessage, "0105");  
    }
    else{
        //sprintf(sysData.debugMessage, "0106");
    }
    
    
    
    //sysData.validMessage = 1;
    messageAvalible = 0;
    sysData.validMessage = validate();
    
    //NVIC_EnableIRQ(USART2_IRQn);
    
    return 1;    
    
}




void CommHandler::send(){
    
    if(!lockOut){
        txIndex = 0;
        
        if(comm.writeable()){
               
            //while(sysData.debugMessage[txIndex] != '\0'){
            //    comm.putc(sysData.debugMessage[txIndex]);
            //    if(txIndex++ > 100){break;}
            //}
            //txIndex = 0;
            
            while(txMessage[txIndex] != '\0'){
               comm.putc(txMessage[txIndex]);
                if(txIndex++ > 100){break;}
            }
        }
        if(messageId++ == 65535){messageId = 0;}               
    }
    
}





