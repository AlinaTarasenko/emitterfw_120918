#ifndef BUZZER_H
#define BUZZER_H

#include "mbed.h"

class Buzzer{
public:
	Buzzer(PinName);

	void update();

	void setFrequency(int Hz);
	void setPulseTime(float onTime, float offTime);

	void start();
	void stop();



private:

	float beepOnTime;
	float beepOffTime;
	bool state;

	Timer buzzerTime;
	PwmOut buzzerPwm;

};


#endif
