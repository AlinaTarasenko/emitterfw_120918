#include "lightController.h"

LightControl::LightControl(PinName laserPin, PinName redPin, PinName greenPin, PinName bluePin, SystemData &sysRef)
: laserEn(laserPin)
, redEn(redPin)
, greenEn(greenPin)
, blueEn(bluePin)
, sysData(sysRef)
{
    laserMode = 0;
    patternMode = 0;
    prevPattern = 0;
    
    laserEn = 0;
    
    blinkTime = 500;
    blink = 0;
    blinkState = 0;
    
    lightTimer.start();

    redEn = 0;
    greenEn = 0;
    blueEn = 0;
    
}



/*
        error check
            connection - blue flashing
            remote system error - yellow/blue
            power supply / xray error / tube temp - red/blue

        xray check
            off - green, laser off
            not armed - yellow flashing, laser off
                too close / no tracking lock
            armed - yellow, laser on
            active - red 
*/


void LightControl::update(){
    
    int newPattern = 0;

    if(sysData.systemState == 0){
        newPattern = 4; laserMode = 0; blink = 0;
    }
    else if(sysData.systemState == 3 && sysData.receivedErrors == 0){
        newPattern = 4; laserMode = 0; blink = 1;
    }
    else if(sysData.systemState == 1 && sysData.receivedErrors == 0){
        newPattern = 7; laserMode = 0; blink = 0;
    }
    else if(sysData.receivedErrors){
        if(sysData.receivedErrors && 0x3){newPattern = 4; laserMode = 0; blink = 0;}
        else if(sysData.receivedErrors && 0x4){newPattern = 4; laserMode = 0; blink = 0;}
        else if(sysData.receivedErrors && 0x8){newPattern = 4; laserMode = 0; blink = 1;}
        else{newPattern = 0; laserMode = 0; blink = 0;}
    }
    else{
        if(sysData.xrayState == 0){newPattern = 0; laserMode = 0; blink = 0;}
        else if(sysData.xrayState == 1){newPattern = 1; laserMode = 0; blink = 1;}
        else if(sysData.xrayState == 2){newPattern = 1; laserMode = 1; blink = 0;}
        else if(sysData.xrayState == 3){newPattern = 2; laserMode = 1; blink = 0;}
        else if(sysData.xrayState == 5){newPattern = 3; laserMode = 0; blink = 1;}
        else{newPattern = 0; laserMode = 0; blink = 0;}        
    }    

    if(lightTimer.read_ms() > blinkTime){
        blinkState = !blinkState;
        lightTimer.reset();
    }    
    
    setPattern(newPattern);
    setLaser();
    
    
}

void LightControl::setLaser(){
    
    if(blink){
        laserMode = laserMode * blinkState;
    }    
    
    laserEn = laserMode;
    
}

void LightControl::setColors(float r, float g, float b, float a){
	redEn = r*a;
	greenEn = g*a;
	blueEn = b*a;
}

void LightControl::setPattern(int newPattern){

    patternMode = newPattern;

    if(blink){
        patternMode = patternMode * blinkState;
    }
    
    
    if( patternMode != prevPattern){
        prevPattern = patternMode;
        
        switch(patternMode){
            case 0: // off
            	setColors(0,0,0,0);
            	sysData.ledState = 0;
                break;
            case 1: // green
            	setColors(0,1,0,1);
            	sysData.ledState = 1;
            	break;
            case 2: // yellow
            	setColors(1,1,0,1);
            	sysData.ledState = 2;
                break;
            case 3: // red
            	setColors(1,0,0,1);
            	sysData.ledState = 3;
                break;
            case 4: // blue
            	setColors(0,0,1,1);
            	sysData.ledState = 4;
                break;
            case 7: // micro c blue
            	setColors(0,0.25,0.5,1);
            	sysData.ledState = 5;
                break;    
            default:
            	setColors(0,0,0,0);
            	sysData.ledState = 0;
                break;
                
            
        }
    
    }
    
    
}
