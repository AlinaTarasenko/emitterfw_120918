/*
 * pca9536.h
 *
 *  Created on: Dec 1, 2017
 *      Author: Igor
 */

#ifndef PCA9536_H_
#define PCA9536_H_

#include "mbed.h"


class pca9536{
public:
	pca9536(I2C&);

	//void init();
	uint8_t readAll();

	void setPolarity(int);
	void pollInputs();

    int trigger();
    int button1();
    int button2();
    int button3();


private:
    uint8_t inputStates;
    uint8_t prevInput;
    int addr;

    uint8_t triggerMask;
    uint8_t button1Mask;
    uint8_t button2Mask;
    uint8_t button3Mask;

	I2C &i2c;
	Timer holdTimer;

};

#endif /* PCA9536_H_ */
