#ifndef APA102_H
#define APA102

#include "mbed.h"
#include <vector>

#define b11100000 0xE0

class apa102{

public:
    apa102(PinName, PinName, int);
    
    uint32_t colorToInt(uint8_t, uint8_t, uint8_t, uint8_t);
    
    void setLed(int, uint8_t, uint8_t, uint8_t, uint8_t);
    
    void setStrip(uint32_t);
    void setStrip(uint32_t*);
    
    void ledCallback();
    
    void write();
    
    int getBit(uint32_t, int);
    
    
    
    
private:

    uint32_t startFrame;
    uint32_t endFrame;
    
    int stripLength;
    
    bool sendData;
    bool clkPh;
    int stripIndex;
    int bitIndex;
    
    union{
      uint32_t i;
      uint8_t a[4];
    } ledBytes;
    
    vector <uint32_t> strip;

    DigitalOut data;
    DigitalOut clk;
    Ticker ioSwitch;
    
    
    
    
    
};



#endif
