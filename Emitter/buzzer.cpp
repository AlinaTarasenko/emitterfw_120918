/*
 * buzzer.cpp
 *
 *  Created on: Feb 6, 2018
 *      Author: Igor
 */

#include "buzzer.h"


Buzzer::Buzzer(PinName buzzerPin) : buzzerPwm(buzzerPin){

	buzzerPwm = 0;
	buzzerTime.start();

	beepOnTime = 1.0;
	beepOffTime = 1.0;
	state = false;

}

void Buzzer::update(){

	float currentTime = buzzerTime.read();

	if(state){
		if(currentTime <= beepOnTime){
			buzzerPwm = 0.5;
		}
		else if(currentTime > beepOnTime && currentTime <= beepOnTime+beepOffTime){
			buzzerPwm = 0;
		}
		else{
			buzzerTime.reset();
		}
	}
	else{
		buzzerPwm = 0;
	}
}

void Buzzer::setFrequency(int Hz){

	buzzerPwm.period(1/(float)Hz);

}

void Buzzer::setPulseTime(float onTime, float offTime){
	beepOnTime = onTime;
	beepOffTime = offTime;
}

void Buzzer::start(){
	state = true;
}

void Buzzer::stop(){
	state = false;
}



