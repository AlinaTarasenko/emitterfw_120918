/*
 * pca9536.cpp
 *
 *  Created on: Dec 1, 2017
 *      Author: Igor
 */

#include "pca9536.h"

pca9536::pca9536(I2C& i2cRef) : i2c(i2cRef){
	// pca9536 i2c 7bit address
	addr = 0x41<<1;
    triggerMask = 0x8; // button 3 1000
    button1Mask = 0x2; // ok
    button2Mask = 0x1; // trigger 0001
    button3Mask = 0x4; // button 2 0100


	inputStates = 0;
	prevInput = 0;

	holdTimer.start();
}


/*
 * Channel polarity -
 * Pol = 0b[x][x][x][x][ch1][ch2][ch3][ch4]
 * 0 = default
 * 1 = inverted
 *
 * Set x to 0
 */
void pca9536::setPolarity(int Pol){
	i2c.start();
	i2c.write(addr & 0xFE);
	i2c.write(0x02);
	i2c.write(Pol);
	i2c.stop();
}

uint8_t pca9536::readAll(){

    uint8_t value;

    i2c.start();
    i2c.write(addr & 0xFE);
    i2c.write(0x00);
    i2c.start();
    i2c.write(addr | 0x01);
    value=i2c.read(0);
    i2c.stop();

    return(value);

	return 0;
}


void pca9536::pollInputs(){
	prevInput = inputStates;
	inputStates = readAll() & 0xF;
}

int pca9536::trigger(){
	int output;

	if( (inputStates & triggerMask) == 0 && (prevInput & triggerMask)  == 0){ output = 0; }     // off
	if( (inputStates & triggerMask) == triggerMask && (prevInput & triggerMask)  == triggerMask){ output = 1; }    // on
	if( (inputStates & triggerMask) == triggerMask && (prevInput & triggerMask)  == 0){ output = 2;  holdTimer.reset();}    // pressed
	if( (inputStates & triggerMask) == 0 && (prevInput & triggerMask)  == triggerMask){ output = 3; }    // released

	if(output == 1 and holdTimer.read() > 1){
		return 4;
	}
	else{
		return output;
	}
}

int pca9536::button1(){
	int output;

	if( (inputStates & button1Mask) == 0 && (prevInput & button1Mask)  == 0){ output = 0; }     // off
	if( (inputStates & button1Mask) == button1Mask && (prevInput & button1Mask)  == button1Mask){ output = 1; }    // on
	if( (inputStates & button1Mask) == button1Mask && (prevInput & button1Mask)  == 0){ output = 2;  holdTimer.reset();}    // pressed
	if( (inputStates & button1Mask) == 0 && (prevInput & button1Mask)  == button1Mask){ output = 3; }    // released

	if(output == 1 and holdTimer.read() > 1){
		return 4;
	}
	else{
		return output;
	}
}

int pca9536::button2(){
	int output;

	if( (inputStates & button2Mask) == 0 && (prevInput & button2Mask)  == 0){ output = 0; }     // off
	if( (inputStates & button2Mask) == button2Mask && (prevInput & button2Mask)  == button2Mask){ output = 1; }    // on
	if( (inputStates & button2Mask) == button2Mask && (prevInput & button2Mask)  == 0){ output = 2;  holdTimer.reset();}    // pressed
	if( (inputStates & button2Mask) == 0 && (prevInput & button2Mask)  == button2Mask){ output = 3; }    // released

	if(output == 1 and holdTimer.read() > 1){
		return 4;
	}
	else{
		return output;
	}
}

int pca9536::button3(){
	int output;

	if( (inputStates & button3Mask) == 0 && (prevInput & button3Mask)  == 0){ output = 0; }     // off
	if( (inputStates & button3Mask) == button3Mask && (prevInput & button3Mask)  == button3Mask){ output = 1; }    // on
	if( (inputStates & button3Mask) == button3Mask && (prevInput & button3Mask)  == 0){ output = 2;  holdTimer.reset();}    // pressed
	if( (inputStates & button3Mask) == 0 && (prevInput & button3Mask)  == button3Mask){ output = 3; }    // released

	if(output == 1 and holdTimer.read() > 1){
		return 4;
	}
	else{
		return output;
	}
}


