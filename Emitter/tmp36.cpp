#include "tmp36.h"


tmp36::tmp36(PinName input) : sensor(input){
    aRef = 3.3;
}

float tmp36::read(){
    float temp = (100*(sensor.read() * aRef)) - 50;
    return temp;
    //return offset(temp); 
    
}


float tmp36::offset(float input){
    
    // y = -0.0033x2 + 1.4052x + 3.8121
    // y = output
    // x = input
    
    
    float output = -0.0033*(input*input) + 1.4052*input + 3.8121;
    return output;
    
        
}
