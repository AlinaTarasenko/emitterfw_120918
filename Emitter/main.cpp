#include "mbed.h"
#include "systemData.h"
#include "states.h"
#include "commHandler.h"
#include "pca9536.h"
#include "lidarManager.h"
#include "Adafruit_GFX\Adafruit_SSD1306.h"
#include "collimator.h"
#include "bno055.h"
#include "Watchdog.h"
#include "tmp36.h"
#include "lightController.h"
#include "buzzer.h"


/*

  https://os.mbed.com/users/mbed_official/code/mbed-src/issues/7


 */

// communications declarations
Serial uart(PA_2, PA_3);
Serial imuComm(PC_10, PC_11);

I2C i2c(PB_9, PB_8);
I2C i2cLidar(PB_3, PB_10);

// system data init
SystemData sysData;
StateData machine(sysData);

// pc communication
CommHandler pcComms(uart, sysData);
Ticker serialSender;
Timer txTimer;
void printBuffer();
void bufferCallBack(){ pcComms.bufferIrq(); }
void senderCallBack(){ printBuffer(); pcComms.send(); }

// sensor and user input setup
pca9536 buttons(i2c);
void inputCallBack(){ buttons.pollInputs();}

tmp36 tempSensor(PC_2);

LidarManager lidar(i2cLidar, PB_14, PB_15, PB_1, uart);

BNO055 imu(imuComm);
vector<double> euler;
DigitalOut bnoReset(PC_12);
void imuCommCallBack(){ imu.bufferIrq(); }
int isIdle(vector<double>);
Timer emitterIdleTime;

// display, LED, and buzzer output
Adafruit_SSD1306_I2c oled(i2c, PC_6, 0x78, 64, 128);
//                        L_PWM, R,    G,     B,
LightControl lightControl(PC_9, PC_7, PC_8, PC_6, sysData);
Buzzer buzzer(PA_5); // Temporary pin for testing, will change later


// Collimator setup
// Arguments: Limit A, Limit B, Motor pwm, Motor direction, Motor sleep, System data)
Collimator collimator(PB_13, PB_12, PB_5, PC_1, PC_0, sysData);
Ticker collimatorUpdate;
void limitSwCallback(){collimator.limitCallback();};
void collimatorCallback(){collimator.update();};

// Timers and ...
Watchdog watchdog;

Timer stateTime;
Timer spinTimer;


// Debugging timestamps
Timer debugTimer;
uint32_t timeStamp;
uint32_t stampSum;
uint32_t stampCounter;

void printBuffer()
{
        pcComms.lockOut = true;
        // sprintf takes about 207 uS
        sprintf(pcComms.txMessage, "#%i,%i,%i,%i,%i,%i,%i,%f,%i,%i,%i,%7.4f,%7.4f,%7.4f,%3.2f,%i,%i,%i~ \n\r",
            pcComms.messageId,  		// 1 int
            sysData.systemState,		// 2 int
            sysData.tubeOverTemp?0:buttons.trigger(),  	// 3 int  if tube is over temp -> send 0, else send trigger state
            buttons.button1(),  		// 4 int
            buttons.button2(),  		// 5 int
            buttons.button3(),  		// 6 int
            sysData.lidarData,  		// 7 int
            sysData.tubeTemp,   		// 8 float
            sysData.xray_kV,    		// 9 int
            sysData.xray_mA,    		// 10 int
            sysData.xray_mS,    		// 11 int
			euler[2],					// 12 float - imu gyro pitch
			euler[1],					// 13 float - imu gyro roll
			euler[0],					// 14 float - imu gyro yaw
			sysData.collimator_position,// 15 float - collimator current position
			sysData.emitterIdle,		// 16 int - is moving?
			sysData.ledState,			// 17 int - led state
			sysData.internalErrors		// 18 int - system error codes
        );
        pcComms.lockOut = false;
}

int main() {

	// i2c clock frequency config (400000 Hz)
	i2c.frequency(400000);

	// collimator setup
	collimator.limitA.rise(&limitSwCallback);
	collimator.limitB.rise(&limitSwCallback);
	collimatorUpdate.attach(&collimatorCallback, 0.001);

	//pc.baud(115200);
	bnoReset = 1;

	// imu comms setup
	imuComm.baud(115200);
	imuComm.attach(&imuCommCallBack, Serial::RxIrq);

	// imu init
	euler.resize(3);

	wait_ms(500);

    if(!imu.begin(BNO055::OPERATION_MODE_NDOF)){
    	sysData.internalErrors |= INTERNAL_ERROR_INIT | INTERNAL_ERROR_IMU; // imu init error
    }

    imu.setExtCrystalUse(false);


	// PC comms setup
	uart.baud(115200);
	uart.attach(&bufferCallBack, Serial::RxIrq);
	serialSender.attach(&senderCallBack, 0.10);
	//txTimer.start();

	// lidar init
	lidar.init();

	// collimator init
	collimator.init();

	// button input config
	buttons.setPolarity(0xF);

	// initial buzzer settings
	buzzer.setFrequency(2500);
	buzzer.setPulseTime(0.5, 1.0);


    // timers and things
    stateTime.start();
    spinTimer.start();

    if(watchdog.WatchdogCausedReset()){
    	sysData.internalErrors |= INTERNAL_ERROR_WATCHDOG; // watchdog triggered reset reset
    }

    //watchdog.Configure(1.0);

    emitterIdleTime.start();
    debugTimer.start();
    stampCounter = 100;
    stampSum = 0;

    while(1) {

    	// Check incoming messages
    	if(pcComms.checkBuffer()){pcComms.parse();}
    	else{sysData.validMessage = pcComms.validate();}
    	sysData.validMessage = 1;

    	// Get system inputs
    	sysData.lidarData = lidar.readSingle();
    	euler = imu.getVector(BNO055::VECTOR_GYROSCOPE);
    	sysData.emitterIdle = isIdle(euler);
    	sysData.tubeTemp = tempSensor.read();
    	buttons.pollInputs();
    	lightControl.update();
    	//buzzer.update();

    	/*
    	 * check temp
    	 * 	if temp > limit && receivedErrors == 0
    	 * 		set xray state to ### (Use to show temp error on display)
    	 * 		start buzzer
    	 * 		override trigger value with 0 (cannot start xray)
    	 *
    	 */

    	// If tube is over temp but NUC has not sent an error
    	if(sysData.tubeTemp > sysData.tempLimit && sysData.receivedErrors == 0){
    		sysData.tubeOverTemp = true;
    		sysData.xrayState = 5; // Override received x-ray state to show "over temp" on OLED nd blink LEDs red
    		buzzer.start();
    	}
    	else{
    		sysData.tubeOverTemp = false;
    		buzzer.stop();
    	}


    	// Reset display
        oled.clearDisplay();
        oled.setTextCursor(0,0);
        oled.setTextColor(1);

        drawFrame();

        machine.stateCheck();

        switch(machine.read() ){
        case msStartup:
            startup();
            //pc.printf("start up \n\r");
            break;

        case msPhoto:
            photo();
            //pc.printf("photo \n\r");
            break;

        case msXray:
            xray();
            //pc.printf("x ray \n\r");
            break;

        case msFault:
            fault();
            break;

        default:
            break;
        }

        //debugTimer.reset();
        // display takes about 2.9 mS
    //    oled.display();
        //timeStamp = debugTimer.read_us();


//        if(stampCounter){
//        	stampSum += timeStamp;
//        	stampCounter--;
//        }
//        else{
//        	stampSum = stampSum/100;
//        	debugTimer.stop();
//        }


        //watchdog.Service();

    }
}

int isIdle(vector<double> gyro){
	if(gyro[0] > 1.0 || gyro[1] > 1.0 || gyro[2] > 1.0 ){
		emitterIdleTime.reset();
		return 0;
	}
	else{
		return (int)emitterIdleTime.read();
	}
}
