#ifndef COMMHAND_H
#define COMMHAND_H

#include "mbed.h"
#include "systemData.h"
#include <string>
#include <vector>


class CommHandler{
    
    public:
    
    CommHandler(Serial&, SystemData&);
    
    //int dataLength;
    //char data[64];    
    //int rxLength;
    //char rxMessage[64];
    char txMessage[256];
    char debugMessage[16];
    
    int connected;
    int messageAvalible;
    
    void send();
    
    void bufferIrq();
    void getBuffer();
    int checkBuffer();
    
    int parse();
    int validate();
    
    
    bool lockOut;
    //private:
    
    vector<int> values;
    
    uint16_t messageId;
    uint16_t recivedId;
    int validMessage;
    
    string incoming;
    
    char buffer[256]; //RX Buffer array
    volatile int index; //RX Buffer index
    volatile int dataAvalible;
    volatile char incomingCh;
    volatile int portActive;
    int txIndex;
    
    //Timer rxTime;
    Timer txTime;
    Timer timeOut;
    Timer timeOutRx;
    
    Serial &comm;
    SystemData &sysData;
    
};


#endif
