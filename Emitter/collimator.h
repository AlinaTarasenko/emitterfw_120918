/*
 * collimator.h
 *
 *  Created on: Dec 7, 2017
 *      Author: Igor
 */

#ifndef COLLIMATOR_H_
#define COLLIMATOR_H_

#define POSITION_ERROR		0x1
#define LIMIT_SW_A_ERROR	0x2
#define LIMIT_SW_B_ERROR	0x4
#define RANGE_ERROR		0x8

#include "mbed.h"
#include "systemData.h"

class Collimator{
public:
	Collimator(PinName, PinName, PinName, PinName, PinName, SystemData&);

	void init();
	void set(float);
	void update();
	float read();
	int ready();

	int getErrors();

	void limitCallback();


	InterruptIn limitA;
	InterruptIn limitB;

private:
	// internal functions
	void encoderInit();
	void encoderReset();
	uint16_t encoderRead();
	int calibrate();


	// motor control loop
	float setPoint;
	float position;

	int motorOpen;
	int motorClose;

	float motorGain;


	// status flags and states
	int calibrated;
	int calibrationState;
	int limitSwitchFlag;

	int errorCode;
	/*
	 * 0b00000001 Position error
	 * 0b00000010 Limit switch A error
	 * 0b00000100 Limit switch B error
	 * 0b00001000 Expected range error
	 */

	// motor position calculations
	float reductionRatio;
	float positionIncrement;
	int revolutionCounter;
	int prevEncoderCount;
	int encoderCount;

	PwmOut motorSpeed;
	DigitalOut motorDir;

	DigitalOut motorSleep;



	SystemData &sysData;

	Ticker updateTimer;
	Timer timer;

};




#endif /* COLLIMATOR_H_ */
