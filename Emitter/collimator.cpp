/*
 * collimator.cpp
 *
 *  Created on: Dec 7, 2017
 *      Author: Igor
 */

#include "collimator.h"


Collimator::Collimator(PinName limA, PinName limB, PinName speed, PinName dir, PinName sleep, SystemData &sysRef)
: limitA(limA)
, limitB(limB)
, motorSpeed(speed)
, motorDir(dir)
, motorSleep(sleep)
, sysData(sysRef)
{
	motorSleep = 0;
	motorSpeed = 0;
	motorDir = 0;

	setPoint = -1;
	position = -1;

	motorGain = 1;

	motorOpen = 0;
	motorClose = 1;

	calibrated = 1; // Set to 1 to skip calibration

	calibrationState = 0;
	limitSwitchFlag = 0;

	//  (1/4096) * (1/19) * (1/7) = 0.000001835643797
	reductionRatio = 0.000001835643797;

	// Max position for encoder counter
	positionIncrement = reductionRatio*65535;

	revolutionCounter = 0;
	prevEncoderCount = 0;
	encoderCount = 0;

	errorCode = 0;

	timer.start();
}

void Collimator::init(){

	/*
	 * init encoder
	 * calibrate motor
	 * check travel
	 * set position to min
	 */

	encoderInit();

	motorSleep = 1;

	while(calibrated != 1){
		calibrate();
		if(errorCode){
			sysData.internalErrors |= INTERNAL_ERROR_INIT | INTERNAL_ERROR_COLLIMATOR;
			break;
		}
	}



}

// Attached to ticker callback - 500 uSeconds
void Collimator::update(){
	/*
	 * update position -
	 * 		read encoder
	 * 		if prev encoder count >= max and encoder <= min ---- increment rotation counter
	 * 		else if  prev encoder count <= min and encoder >= max ---- decrement rotation counter
	 *
	 *		(1/4096) * (1/19) * (1/7) = 0.000001835643797
	 */

	prevEncoderCount = encoderCount;
	encoderCount = encoderRead();

	if(prevEncoderCount <= 1024 && encoderCount >= 64511){revolutionCounter--;}
	else if(prevEncoderCount >= 64511 && encoderCount <= 1024){revolutionCounter++;}

	position = (encoderCount*reductionRatio) + (revolutionCounter*positionIncrement);

	/*
	 * update motor outputs -
	 * 		if setPoint and setpoint - position > %
	 * 		dir =
	 * 		speed = setpoint - position * gain
	 */

	if(setPoint && abs(setPoint - position) > 0.05 ){
		// Check direction here
		motorDir = (setPoint > position) ? motorClose : motorOpen;
		motorSpeed = abs(setPoint - position)*motorGain;
		if(timer.read() > 5.0){
			errorCode |= POSITION_ERROR;
		}
	}

	if(errorCode){
		motorSpeed = 0;
		motorSleep = 0;
		sysData.internalErrors |= INTERNAL_ERROR_COLLIMATOR;
	}


}

void Collimator::set(float target){
	setPoint = target;
	timer.reset();
}

float Collimator::read(){
	// returns position from 0 to 1 as scaled
	return position * (360.0/43.5);
}

int Collimator::ready(){

	if(setPoint && abs(setPoint - position) < 0.05){
		return 1;
	}
	else return 0;
}


int Collimator::calibrate(){

	switch(calibrationState){
	case 0:
		/*
		 * Check if gear is at limit
		 * If yes, move to next step
		 * If no, skip next couple steps
		 */
		if(limitA.read() == 1){
			calibrationState++;
		}
		else{
			calibrationState = 3;
		}
		break;

	case 1:
		/*
		 * Reset timer
		 * set motor direction
		 * set motor speed to low value
		 * move forward to next step
		 */
		timer.reset();
		motorDir = motorClose;
		motorSpeed = 0.1;
		calibrationState++;
		break;

	case 2:
		/*
		 * wait 1 second, stop motor and move on
		 */
		if(timer.read() > 1){
			motorSpeed = 0;
			calibrationState++;
		}
		break;

	case 3:
		/*
		 * set motor direction
		 * set lower speed
		 * reset timer
		 * continue
		 */
		motorDir = motorOpen;
		motorSpeed = 0.05;
		calibrationState++;
		timer.reset();
		break;

	case 4:
		/*
		 * wait for limit switch to close
		 * or wait for timer to reach 2.5 seconds
		 * if timer expires, stop motor, set an error
		 */
		if(limitSwitchFlag){
			limitSwitchFlag = 0;
			encoderReset();
			position = 0;
			calibrationState++;
		}
		else if(timer.read() > 2.5){
			motorSpeed = 0;
			motorSleep = 0;
			// Time out
			// Set error - failure on limit switch A
			errorCode |= LIMIT_SW_A_ERROR;
			calibrationState = 10;
		}
		break;

	case 5:
		/*
		 * change motor direction
		 * reset timer and start motor at low speed
		 */
		motorDir = motorClose;
		motorSpeed = 0.05;
		calibrationState++;
		timer.reset();
		break;

	case 6:
		/*
		 * wait for limit switch to close or timeout
		 */
		if(limitSwitchFlag){
			limitSwitchFlag = 0;
			calibrationState++;
		}
		else if(timer.read() > 5){
			motorSpeed = 0;
			motorSleep = 0;
			// Time out
			// Set error - failure on limit switch B
			errorCode |= LIMIT_SW_B_ERROR;
			calibrationState = 10;
		}
		break;

	case 7:
		/*
		 * read position of motor
		 * if position is not within 5% of expected, set an error
		 */
		if(abs(read() - 1) > 0.05){
			motorSpeed = 0;
			motorSleep = 0;
			calibrationState = 10;
			// set error - range error
			errorCode |= RANGE_ERROR;
		}
		else {
			calibrationState++;
		}
		break;

	case 8:
		/*
		 * set position to 0.05
		 * if position is reached, move forward
		 * if timeout, set error
		 */
		set(0.05);
		timer.reset();
		if(ready()){
			calibrationState++;
		}
		else if(timer.read() > 2.5){
			motorSpeed = 0;
			motorSleep = 0;
			calibrationState = 10;
			// set error - move to target timeout
			errorCode |= POSITION_ERROR;
		}
		break;

	case 9:
		/*
		 * calibration complete
		 * set calibrated to 1
		 */
		calibrationState = 0;
		calibrated = 1;
		break;

	case 10:
		/*
		 * calibration failure
		 */
		calibrated = -1;
		calibrationState = 0;
		break;

	default:
		calibrationState = 0;
		break;

	}

	return calibrated;
}

void Collimator::encoderInit(){

    // configure GPIO PA0 & PA1 as inputs for Encoder
    RCC->AHB1ENR |= 0x00000001;  // Enable clock for GPIOA

    GPIOA->MODER   |= GPIO_MODER_MODER0_1 | GPIO_MODER_MODER1_1 ;           //PA0 & PA1 as Alternate Function   /*!< GPIO port mode register,               Address offset: 0x00      */
    GPIOA->OTYPER  |= GPIO_OTYPER_OT_0 | GPIO_OTYPER_OT_1 ;                 //PA0 & PA1 as Inputs               /*!< GPIO port output type register,        Address offset: 0x04      */
    GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR0 | GPIO_OSPEEDER_OSPEEDR1 ;     // Low speed                        /*!< GPIO port output speed register,       Address offset: 0x08      */
    GPIOA->PUPDR   |= GPIO_PUPDR_PUPDR0_1 | GPIO_PUPDR_PUPDR1_1 ;           // Pull Down                        /*!< GPIO port pull-up/pull-down register,  Address offset: 0x0C      */
    GPIOA->AFR[0]  |= 0x00000011 ;                                          //  AF01 for PA0 & PA1              /*!< GPIO alternate function registers,     Address offset: 0x20-0x24 */
    GPIOA->AFR[1]  |= 0x00000000 ;                                          //                                  /*!< GPIO alternate function registers,     Address offset: 0x20-0x24 */

    // configure TIM2 as Encoder input
    RCC->APB1ENR |= 0x00000001;  // Enable clock for TIM2

    TIM2->CR1   = 0x0001;     // CEN(Counter ENable)='1'     < TIM control register 1
    TIM2->SMCR  = 0x0003;     // SMS='011' (Encoder mode 3)  < TIM slave mode control register
    TIM2->CCMR1 = 0xF1F1;     // CC1S='01' CC2S='01'         < TIM capture/compare mode register 1
    TIM2->CCMR2 = 0x0000;     //                             < TIM capture/compare mode register 2
    TIM2->CCER  = 0x0011;     // CC1P CC2P                   < TIM capture/compare enable register
    TIM2->PSC   = 0x0000;     // Prescaler = (0+1)           < TIM prescaler
    TIM2->ARR   = 0xffffffff; // reload at 0xfffffff         < TIM auto-reload register

    TIM2->CNT = 0x0000;  //reset the counter before we use it

}

void Collimator::encoderReset(){
	TIM2->CNT= 0; //reset count to zero
}

uint16_t Collimator::encoderRead(){
	// return encoder counter as unsigned int
	return (uint16_t)TIM2->CNT + 32768;
}

void Collimator::limitCallback(){
	limitSwitchFlag = 1;
	motorSpeed = 0;
}
