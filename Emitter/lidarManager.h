/*
 * lidarManager.h
 *
 *  Created on: Dec 4, 2017
 *      Author: Igor
 */

#ifndef LIDARMANAGER_H_
#define LIDARMANAGER_H_

#include "mbed.h"
#include "VL53L0X.h"
#include <algorithm>

class LidarManager{
public:
	LidarManager(I2C&, PinName, PinName, PinName, Serial&);
	void init();
	void startContinuous();
	void stopContinuous();
	int readContinuous();
	int readSingle();

private:

	int data1;
	int data2;
	int data3;

	// ADDRESS_DEFAULT (0b0101001 << 1)
	uint8_t address1;
	uint8_t address2;
	uint8_t address3;

	Timer timer;

	VL53L0X lidar1;
	VL53L0X lidar2;
	VL53L0X lidar3;

	DigitalOut shutDown1;
	DigitalOut shutDown2;
	DigitalOut shutDown3;

};


#endif /* LIDARMANAGER_H_ */
