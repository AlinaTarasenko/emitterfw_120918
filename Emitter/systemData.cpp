#include "systemData.h"

SystemData::SystemData(){

    // internally set values:
    systemState = 0;
    // x ray settings
    xray_kV = 30;
    xray_mA = 50;
    xray_mS = 33;

    // lidar settings
    lidarData = 0;
    lidarLimit = 0;

    tempLimit = 55.0;
    tubeTemp = 0;
    emitterIdle = 0;
    internalErrors = 0;
    
    // received values
    mode = 0;
    xrayState = 0;
    xray_Kv_echo = 0; 
    xray_mA_echo = 0;
    xray_mS_echo = 0;       
    receivedErrors = 0;
    
    messageDiff = 0;
    validMessage = 0;
    connected = 0;
    
    debugId = 0;
    
}
