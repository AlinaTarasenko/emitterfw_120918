#include "apa102.h"


apa102::apa102(PinName _data, PinName _clk, int length) : data(_data), clk(_clk){

    data = 0;
    clk = 0;
    stripLength = length;
    
    startFrame = 0;
    endFrame = 4294967295;
    
    sendData = false;
    clkPh = false;
    strip.resize(stripLength+2);
    //printf("set callback \n\r");
    ioSwitch.attach_us( Callback<void()>( this, &apa102::ledCallback), 15);

    
    
}
    
    
void apa102::setLed( int pos, uint8_t a, uint8_t b, uint8_t g, uint8_t r){
    
    ledBytes.a[0] = a + 0xE0;
    ledBytes.a[1] = b;
    ledBytes.a[2] = g;
    ledBytes.a[3] = r;
    
    strip[pos] = ledBytes.i;

}

uint32_t apa102::colorToInt(uint8_t a, uint8_t b, uint8_t g, uint8_t r){
    
    uint32_t color = 0;
    
    color += (a | 0xE0)<<24;
    color += b<<16;
    color += g<<8;
    color += r;
    
    //printf("color set: %u \n\r", color);
    
    return color;
}
    

void apa102::setStrip(uint32_t color){
    strip[0] = startFrame;
    for(int i = 0; i < stripLength; i++){
        strip[i+1] = color;    
    }
    strip[stripLength+1] = endFrame;
    
    for(int i = 0; i < strip.size(); i++){
        //printf("id: %i  value: %u \n\r", i+1, strip[i]);    
    }
}


void apa102::setStrip(uint32_t* colorArray){
    strip[0] = startFrame;
    for(int i = 1; i < stripLength-1; i++){
        strip[i] = colorArray[i];    
    }
    strip[stripLength] = endFrame;    
}    
    
    
void apa102::write(){
    stripIndex = 0;
    bitIndex = 31;
    sendData = true;  
    //printf("send data\n\r");  
}   


int apa102::getBit(uint32_t value, int pos){
    return value>>pos & 1;    
}

    
void apa102::ledCallback(){
    if(sendData){
        
        clkPh = !clkPh;
        
        clk = clkPh;
        
        if(clkPh){
            data = getBit(strip[stripIndex], bitIndex);
        
            //printf("S %i | B %i | V %i\n\r", stripIndex, bitIndex, getBit(strip[stripIndex], bitIndex));
            if(bitIndex-- == 0){
                if(stripIndex++ == stripLength+1){sendData = false;}
                bitIndex = 31;
            }
        }
        
    } 
    else{clk = 0; data = 0;} 
}