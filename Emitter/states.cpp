#include "states.h"
#include "Adafruit_GFX\Adafruit_SSD1306.h"
#include "pca9536.h"
#include "commHandler.h"

StateData::StateData(SystemData &sysRef) : sysData(sysRef){
    

    currentState = msStartup;
    nextState = msStartup; 
    holdState = msStartup;
    
    // UI control
    firstRun = 1;
    selectionId = 0;
    buttonHoldTime = 0;
    
}
    
/*
    Set next machine state.
        Output: None
        Input: Next state, State enum
*/    
void StateData::setState(State_ next){
    nextState = next; 
   // writeState();
}


void StateData::stateCheck(){
    
    //sprintf(sysData.debugMessage, "0200");
    
    if (currentState == msFault){
        if(sysData.validMessage && sysData.receivedErrors == 0){
            nextState = holdState;
        }
        else{nextState = msFault;}
    }
    else{
        if(sysData.validMessage == 0 && sysData.connected == 1){   // 
            holdState = currentState;
            nextState = msFault;
        }
        else if(sysData.receivedErrors){
        	if(sysData.internalErrors && sysData.receivedErrors == ERROR_CLEAR_INTERNAL){
        		sysData.internalErrors = 0;
        	}
            holdState = currentState;
            nextState = msFault;
        }
        else{
            //nextState = nextState;
        }
    }


    //if state changes    
    if(currentState != nextState){
        writeState();
    }
    
    //sprintf(sysData.debugMessage, "0201");

}


void StateData::writeState(){
    currentState = nextState; 
    firstRun = 1;
    }
    
/* 
    Get current machine state.
        Output: State enum
        Input: None
*/
State_ StateData::read(){
    return currentState; 
    }

State_ StateData::next(){
    return nextState; 
    
}
    

extern StateData machine;
extern SystemData sysData;
extern CommHandler pcComms;
extern pca9536 buttons;
extern Adafruit_SSD1306_Spi oled;
extern Timer stateTime;  
extern Timer spinTimer;
 
        
        
        
void startup(){
    if(machine.firstRun){
        machine.firstRun = 0;  
        stateTime.reset();  
        sysData.systemState = 0;
    }
    
    oled.clearDisplay();
    oled.logo();
    
    if(sysData.validMessage == 1 && stateTime.read() > 5){
        sysData.connected = 1;
        machine.setState(msPhoto);
    }
    //pc.printf("state time: %f \n\r", stateTime.read());

    
}


/*
photo -
no options
*/

void photo(){
    
    if(machine.firstRun){    
        machine.firstRun = 0;
        machine.selectionId = 0;
        sysData.systemState = 1;
    }    
    
    if(buttons.button2() == 2){
        machine.selectionId++;
        if(machine.selectionId == 1){machine.selectionId = 0;}
    }    

    // draw stuff on screen depending on which item is selected
    // button 1 and 3 adjust value of selected item
    
    switch(machine.selectionId){
    case 0: // mode select
        
        if(buttons.button1() == 2){
           machine.setState(msXray);
        }      
        if(buttons.button3() == 2){
            machine.setState(msXray);
        }                
        break;

    default:
        break;     
    }
    
    drawPhoto(machine.selectionId);
    
            
}



void drawPhoto(int selection){
        
    oled.setTextCursor(18,22);
    oled.setTextSize(2); 
    oled.printf("Camera"); 
    
    oled.setTextCursor(10,39);
    oled.setTextSize(1);   
    oled.printf("Trigger for photo");
          
    oled.fillRect(0, 53, 128, 10, 1);
    oled.setTextCursor(8,55);
    oled.setTextColor(0);
    oled.setTextSize(1); 
    oled.printf("Press < > for x-ray");  //Press < > to enable

       
}


/*
void drawPhoto(int selection){
    
    int xOffset = -8;
    
    
    oled.setTextCursor(16+xOffset,25);
    oled.setTextSize(3); 
    oled.printf("Camera");   
          
    oled.fillRect(0, 53, 128, 10, 1);
    oled.setTextCursor(38,55);
    oled.setTextColor(0);
    oled.setTextSize(1); 
    oled.printf("Camera");  

       
}
*/




/*
xray

kV
mA
mS

*/
void xray(){
    
    if(machine.firstRun){
        machine.firstRun = 0;
        sysData.systemState = 2;
        stateTime.reset(); 
    }    
    
    
    if(buttons.button2() == 2){
        machine.selectionId++;
        if(machine.selectionId == 4){machine.selectionId = 0;}
    }    
      
      
    switch(machine.selectionId){
    case 0: // mode select
        
        if(buttons.button1() == 2){
           machine.setState(msPhoto);
        }      
        if(buttons.button3() == 2){
            machine.setState(msPhoto);
        }        
        
        
        break;
    case 1: // voltage select 
         if(buttons.button1() == 2){
           sysData.xray_kV -= 10;
           if(sysData.xray_kV < 30){sysData.xray_kV = 30;}
           
        }          
        if(buttons.button3() == 2){
            sysData.xray_kV += 10;
            if(sysData.xray_kV > 60){sysData.xray_kV = 60;}
            
        }      
            
        if(buttons.button1() == 4){
           sysData.xray_kV = timedDec(sysData.xray_kV, 10);
           if(sysData.xray_kV < 30){sysData.xray_kV = 30;}
        }              
        if(buttons.button3() == 4){
           sysData.xray_kV = timedInc(sysData.xray_kV, 10);
           if(sysData.xray_kV > 60){sysData.xray_kV = 60;}
        }             
        
        
        
        break;

     case 2: // current select 
        if(buttons.button1() == 2){
           sysData.xray_mA -= 25;
           if(sysData.xray_mA < 50){sysData.xray_mA = 50;}
        }     
        if(buttons.button3() == 2){
            sysData.xray_mA += 25;
            if(sysData.xray_mA > 100){sysData.xray_mA = 100;}
        }     
        
        if(buttons.button1() == 4){
           sysData.xray_mA = timedDec(sysData.xray_mA, 25);
           if(sysData.xray_mA < 50){sysData.xray_mA = 50;}
        }              
        if(buttons.button3() == 4){
           sysData.xray_mA = timedInc(sysData.xray_mA, 25);
           if(sysData.xray_mA > 100){sysData.xray_mA = 100;}
        }          
        
             
        break;      
        
     case 3: // expoure time select 
         if(buttons.button1() == 2){
           sysData.xray_mS -= 33;
           if(sysData.xray_mS < 33){sysData.xray_mS = 33;}
        }      
        if(buttons.button3() == 2){
            sysData.xray_mS += 33;
            if(sysData.xray_mS > 99){sysData.xray_mS = 99;}
        }      
        
        if(buttons.button1() == 4){
           sysData.xray_mS = timedDec(sysData.xray_mS, 22);
           if(sysData.xray_mS < 33){sysData.xray_mS = 33;}
        }              
        if(buttons.button3() == 4){
           sysData.xray_mS = timedInc(sysData.xray_mS, 22);
           if(sysData.xray_mS > 99){sysData.xray_mS = 99;}
        }           
        
            
        break;       
        
    default:
        break;       
    }
    
    //sprintf(sysData.debugMessage, "0202");
    drawXray(machine.selectionId);
    
       
}


void drawXray(int selection){


    oled.drawLine(65,17,65,53,1);   
    oled.drawLine(65,35,127,35,1); 
    
    
    oled.setTextCursor(3,25);
    oled.setTextSize(3); 
    oled.printf("%i", sysData.xray_kV);
    oled.setTextSize(2);
    oled.printf("kV");

    oled.setTextCursor(70,19);
    oled.setTextSize(2); 
    oled.printf("%03i", sysData.xray_mA);   
    oled.setTextSize(1);
    oled.printf("mA");
    oled.fillRect(79, 32, 2, 2, 1);
    
    oled.setTextCursor(70,38);
    oled.setTextSize(2); 
    oled.printf("%i", sysData.xray_mS);   
    oled.setTextSize(1);
    oled.printf("mS");    
    
    oled.setTextCursor(50,55);
    oled.setTextSize(1); 
    oled.printf("X-Ray");  
    
    //fillRect(uint16_t x0, uint16_t y0, uint16_t w, uint16_t h, uint16_t color);
    
    switch(selection){
    case 0: // mode select
        oled.fillRect(0, 53, 128, 10, 1);
        
        oled.setTextCursor(50,55);
        oled.setTextColor(0);
        oled.setTextSize(1); 
        oled.printf("X-Ray");  
        break;
    
    case 1: // kv
        oled.fillRect(0, 17, 65, 36, 1); 
        
        oled.setTextCursor(3,25);
        oled.setTextColor(0);
        oled.setTextSize(3); 
        oled.printf("%i", sysData.xray_kV);
        oled.setTextSize(2);
        oled.printf("kV");

        break;
       
    case 2: // ma
    
        oled.fillRect(65, 17, 63, 18, 1); 
        
        oled.setTextCursor(70,19);
        oled.setTextColor(0);
        oled.setTextSize(2); 
        oled.printf("%03i",sysData.xray_mA);   
        oled.setTextSize(1);
        oled.printf("mA");  
        oled.fillRect(79, 32, 2, 2, 0);     

        break;         

    case 3: // ms    
        oled.fillRect(65, 35, 63, 18, 1); 
        
        oled.setTextCursor(70,38);
        oled.setTextColor(0);
        oled.setTextSize(2); 
        oled.printf("%i", sysData.xray_mS);   
        oled.setTextSize(1);
        oled.printf("mS");       

        break;         

    
    default:
        break;
    }
    //sprintf(sysData.debugMessage, "0203");

}

void fault(){
    
    if(machine.firstRun){
        machine.firstRun = 0;
        sysData.systemState = 3;
        stateTime.reset(); 
    }
    
    oled.setTextCursor(36,22);
    oled.setTextSize(2); 
    oled.printf("Error"); 
    
    oled.setTextCursor(9,39);
    oled.setTextSize(1);     
    
    if(sysData.receivedErrors & ERROR_TEMP){
        oled.printf("Tube temp");
    }
    else if(sysData.receivedErrors & ERROR_HV_COMMUNICATION){
        oled.printf("HV Supply");
    }
    else if(sysData.receivedErrors & ERROR_DET_COMMUNICATION){
        oled.printf("Detector");
    }
    else if(sysData.receivedErrors & ERROR_TRACKING){
        oled.printf("Tracking");
    }
    else if(sysData.receivedErrors & ERROR_CLEAR_INTERNAL){
        oled.printf("Emitter");
    }
    else if(sysData.receivedErrors == 0){
        oled.printf("Connection");
    }    
    else{
        oled.printf("Unknown %i", sysData.receivedErrors);
    }
                    

    oled.setTextCursor(50,55);
    oled.setTextSize(1); 
    oled.printf("Error"); 
    
        
    /*
    if(sysData.errorCode && 0b00000000){oled.printf("Something \n\r");}
    
    */    
    
}


int timedInc(int input, int value){
    if(stateTime.read() > 0.20f){
        stateTime.reset();
        input = input + value;
    }    
    return input;
}


int timedDec(int input, int value){
    if(stateTime.read() > 0.20f){
        stateTime.reset();
        input = input - value;
    }    
    return input;
}

void drawFrame(){
    
    static int spinState = 0;
    
    // center marker
    // oled.drawLine(0,32,127,32,1);   
    // oled.drawLine(64,0,64,63,1);
    // oled.drawCircle(64,32,5,1);
    
    oled.drawLine(0,17,127,17,1);   
    oled.drawLine(0,53,127,53,1); 
    
    
    
    oled.setTextColor(1);
    oled.setTextSize(2);
    
    int xOffset = -7;
    
    switch(sysData.xrayState){
    //switch(10){
    case 0:
        oled.setTextCursor(20+xOffset,2);
        oled.printf("Disarmed");
        break;
    case 1:
        oled.setTextCursor(26+xOffset,2);
        oled.printf("STANDBY");        
        break;
    case 2:
        oled.setTextCursor(37+xOffset,2);
        oled.printf("ARMED");    
        break;
    case 3:
        oled.setTextCursor(31+xOffset,2);
        oled.printf("ACTIVE");    
        break;
    case 5:
        oled.setTextCursor(20+xOffset,2);
        oled.printf("OVER TEMP.");
        break;
    case 10:
        oled.setTextSize(1);
        oled.setTextCursor(20+xOffset,2);
        //oled.printf("%i|%i", pcComms.recivedId, pcComms.messageId);
        oled.printf("%i|%i|%i",pcComms.messageId, pcComms.messageId - pcComms.recivedId, sysData.validMessage);
        break;
    default:
        oled.setTextCursor(37+xOffset,2);
        oled.printf("ERROR");    
        break;    
        
    }
    
    oled.setTextSize(1);
    oled.setTextCursor(2,5);
    if(spinTimer.read() >= 0.25f){
        if(spinState++ == 3){spinState = 0;}
        spinTimer.reset();
    }
    switch(spinState){
    case 0:
        oled.printf("-");
        break;
    case 1:
        oled.printf("/");
        break;
    case 2:
        oled.printf("|");
        break;
    case 3:
        oled.printf("\\");
        break;
    default:
        break;    
        
    }
    /*
    xRay state - bold centered top " off | armed | ready | active "
    LINE
    
    state section
    
    LINE
    
    state indicator
    
    */
     
    
}




