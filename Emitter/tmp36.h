#ifndef TMP36_H
#define TMP36_H

#include "mbed.h"

class tmp36{
    
    public:
    tmp36(PinName);

    float read();    
    float offset(float);
    
    
    private:
    float aRef;
    AnalogIn sensor;
    
};


#endif

